import json
import sys
import csv

filename = sys.argv[2]
if sys.argv[1] == "-b":
    filename2 = filename.replace('csv', 'json')
    with open(filename) as file_read:
        contents = list(csv.reader(file_read))

    with open(filename2, "w") as file_write:
        dict_list = []
        for i in range(1, len(contents)):
            space_values = contents[i]
            values = []
            for value in space_values:
                values.append(value.strip())
            if values[9] == '' and values[10] == '':
                device_dict = {
                    'id': values[0],
                    'name': values[1],
                    'type': values[2],
                    'hardware': {
                        'name': values[3],
                        'serial': values[4],
                    },
                    'software': {
                        'version': values[5],
                        'update_date': values[6],
                    },
                    'network': [
                        {
                            'mac': values[7],
                            'ipv4': values[8]
                        },
                    ],
                    'location': values[11],
                    'state': values[12]
                }
            else:
                device_dict = {
                    'id': values[0],
                    'name': values[1],
                    'type': values[2],
                    'hardware': {
                        'name': values[3],
                        'serial': values[4],
                    },
                    'software': {
                        'version': values[5],
                        'update_date': values[6],
                    },
                    'network': [
                        {
                            'mac': values[7],
                            'ipv4': values[8]
                        },
                        {
                            'mac': values[9],
                            'ipv4': values[10]
                        }
                    ],
                    'location': values[11],
                    'state': values[12]
                }

            dict_list.append(device_dict)
        json.dump(dict_list, file_write, indent=2)

if sys.argv[1] == "-p":
    filename2 = filename.replace('json', 'csv')
    with open("devices.json") as file_read:
        contents = json.load(file_read)
        devices_list = [
            ['id   ', 'name   ', 'type  ', 'hardware_name ', 'hardware_serial', 'software_version', 'software_update_date',
             'network1_mac     ', 'network1_ipv4', 'network2_mac     ', 'network2_ipv4', 'location', 'state']
        ]
        for content in contents:
            if len(content['network']) > 1:
                device_list = [
                    content['id'].ljust(5),
                    content['name'].ljust(7),
                    content['type'].ljust(6),
                    (content['hardware'])['name'].ljust(14),
                    (content['hardware'])['serial'].ljust(15),
                    (content['software'])['version'].ljust(16),
                    (content['software'])['update_date'].ljust(20),
                    ((content['network'])[0])['mac'].ljust(17),
                    ((content['network'])[0])['ipv4'].ljust(13),
                    ((content['network'])[1])['mac'].ljust(17),
                    ((content['network'])[1])['ipv4'].ljust(13),
                    content['location'].ljust(8),
                    content['state'].ljust(5)
                ]
            else:
                device_list = [
                    content['id'].ljust(5),
                    content['name'].ljust(7),
                    content['type'].ljust(6),
                    (content['hardware'])['name'].ljust(14),
                    (content['hardware'])['serial'].ljust(15),
                    (content['software'])['version'].ljust(16),
                    (content['software'])['update_date'].ljust(20),
                    ((content['network'])[0])['mac'].ljust(17),
                    ((content['network'])[0])['ipv4'].ljust(13),
                    '                 ',
                    '             ',
                    content['location'].ljust(8),
                    content['state'].ljust(5)
                ]

            devices_list.append(device_list)

    with open(filename2, 'w', newline='') as file_write:
        writer = csv.writer(file_write)
        for line in devices_list:
            writer.writerow(line)
