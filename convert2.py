import json
import sys

import pandas as pd

filename = sys.argv[2]

if sys.argv[1] == "-b":
    filename2 = filename.replace('csv', 'json')
    df = pd.read_csv(filename, dtype='str')
    result = df.to_json(orient='records')
    parsed = json.loads(result)
    with open(filename2, "w") as file_write:
        json.dump(parsed, file_write, indent=4)

if sys.argv[1] == "-p":
    filename2 = filename.replace('json', 'csv' )
    df = pd.read_json(filename)
    df.to_csv(filename2, index='False')

